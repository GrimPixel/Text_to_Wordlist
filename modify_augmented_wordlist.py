from csv import reader
from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")


def l_get_sounds_or_synonyms_filtered_entry(l_entry, l_included_tags):
    l_filtered_entry = []
    for s_entry in l_entry:
        l_entry_component = s_entry.split(" • ")
        l_filtered_entry_component = []
        for s_entry_component in l_entry_component:
            if " : " not in s_entry_component:
                continue
            s_description, s_content = s_entry_component.split(" : ")
            l_description_component = s_description.split(" · ")
            if l_description_component in l_included_tags:
                l_filtered_entry_component.append(s_entry_component)
        s_filtered_entry_component = " • ".join(l_filtered_entry_component)
        l_filtered_entry.append(s_filtered_entry_component)
    return l_filtered_entry


def l_get_translations_filtered_entry(l_entry):
    l_filtered_entry = []
    for s_entry in l_entry:
        l_group = s_entry.split(" | ")
        l_filtered_group = []
        for s_group in l_group:
            if " : " not in s_group:
                continue
            s_description, s_content = s_group.split(" : ")
            l_description_component = s_description.split(" • ")
            for s_description_component in l_description_component:
                if s_description_component in d_setting["translations included tags"]:
                    l_filtered_group.append(s_group)
                    break
        s_filtered_group = " | ".join(l_filtered_group)
        l_filtered_entry.append(s_filtered_group)
    return l_filtered_entry


def l_get_senses_filtered_entry(l_entry):
    l_filtered_entry = []
    i_gloss_limit = d_setting["glosses/raw_glosses limit"]
    for s_entry in l_entry:
        l_entry_component = s_entry.split(" • ")
        l_filtered_entry_component = []
        for s_entry_component in l_entry_component:
            if i_gloss_limit <= 0:
                break
            l_filtered_entry_component.append(s_entry_component)
            i_gloss_limit -= 1
        s_filtered_entry = " • ".join(l_filtered_entry_component)
        l_filtered_entry.append(s_filtered_entry)
    return l_filtered_entry


#
def l_get_merged_glosses_filtered_line_component(
    l_filtered_line_component, l_header_component
):
    i_raw_glosses_index = l_header_component.index("ld:senses ls:raw_glosses")
    i_glosses_index = l_header_component.index("ld:senses ls:glosses")

    i_filtered_line_component_amount = len(l_filtered_line_component)
    for i_filtered_line_component_index in range(i_filtered_line_component_amount):
        s_raw_glosses_entry = l_filtered_line_component[i_raw_glosses_index]
        l_raw_glosses_group = s_raw_glosses_entry.split(" ‖ ")
        s_glosses_entry = l_filtered_line_component[i_glosses_index]
        l_glosses_group = s_glosses_entry.split(" ‖ ")

        i_glosses_group_amount = len(l_glosses_group)
        for i_glosses_group_index in range(i_glosses_group_amount):
            s_raw_glosses_group = l_raw_glosses_group[i_glosses_group_index]
            s_glosses_group = l_glosses_group[i_glosses_group_index]

            l_raw_gloss_in_group = s_raw_glosses_group.split(" • ")
            l_gloss_in_group = s_glosses_group.split(" • ")

            i_gloss_in_group_amount = len(l_gloss_in_group)
            for i_raw_gloss_in_group_index in range(i_gloss_in_group_amount):
                s_raw_gloss = l_raw_gloss_in_group[i_raw_gloss_in_group_index]
                if s_raw_gloss == "":
                    continue
                l_gloss_in_group[i_raw_gloss_in_group_index] = s_raw_gloss
            s_glosses_group = " • ".join(l_gloss_in_group)
            l_glosses_group[i_glosses_group_index] = s_glosses_group
        l_filtered_line_component[i_glosses_index] = " ‖ ".join(l_glosses_group)
    if b_remove_raw_glosses_after_merging:
        l_filtered_line_component.pop(i_raw_glosses_index)
    return l_filtered_line_component


s_setting_file = "setting.yaml"
p_setting_file = Path(s_setting_file)
s_setting_text = p_setting_file.read_text()
d_setting = yaml.load(s_setting_text)

s_augmented_wordlist_directory = d_setting["augmented wordlist directory"]
p_augmented_wordlist_directory = Path(s_augmented_wordlist_directory)
s_filtered_augmented_wordlist_directory = d_setting[
    "filtered augmented wordlist directory"
]
p_filtered_augmented_wordlist_directory = Path(s_filtered_augmented_wordlist_directory)
p_filtered_augmented_wordlist_directory.mkdir(exist_ok=True)

l_dictionary_content_with_type = d_setting["dictionary content with type"]
l_dictionary_content_with_type_after_filtering = d_setting[
    "dictionary content with type after filtering"
]

b_merge_non_empty_raw_glosses_into_glosses = d_setting[
    "merge non-empty raw_glosses into glosses"
]
b_remove_raw_glosses_after_merging = (
    "ld:senses ls:raw_glosses" in l_dictionary_content_with_type
    and "ld:senses ls:raw_glosses" not in l_dictionary_content_with_type_after_filtering
)

s_header_after_filtering = "\t".join(l_dictionary_content_with_type_after_filtering)
l_header_after_filtering_component = s_header_after_filtering.split("\t")

for p_augmented_wordlist_file in p_augmented_wordlist_directory.rglob("*.tsv"):
    s_augmented_wordlist_file_path = str(p_augmented_wordlist_file)
    with open(s_augmented_wordlist_file_path) as w_augmented_wordlist_file_path:
        r_augmented_wordlist_file_path = reader(
            w_augmented_wordlist_file_path, delimiter="\t"
        )

        l_filtered_line = []
        l_header_component = next(r_augmented_wordlist_file_path)
        i_header_component_amount = len(l_header_component)
        for l_line_component in r_augmented_wordlist_file_path:
            l_filtered_line_component = []
            for i_header_component_index in range(i_header_component_amount):
                s_header_component = l_header_component[i_header_component_index]
                if (
                    s_header_component
                    not in l_dictionary_content_with_type_after_filtering
                    and not (
                        s_header_component == "ld:senses ls:raw_glosses"
                        and b_remove_raw_glosses_after_merging
                    )
                ):
                    continue

                s_word_all_entry = l_line_component[i_header_component_index]
                l_entry = s_word_all_entry.split(" ‖ ")
                if (
                    s_header_component
                    in (
                        "ld:sounds ls:tags,s:ipa",
                        "ld:sounds ls:tags,s:zh_pron",
                        "ld:sounds ls:tags,s:zh-pron",
                        "ld:sounds ls:raw_tags,s:ipa",
                        "ld:sounds ls:raw_tags,s:zh_pron",
                        "ld:sounds ls:raw_tags,s:zh-pron",
                    )
                    and d_setting["filter sounds"]
                ):
                    l_included_tags = d_setting["sounds included tags"]
                    l_filtered_entry = l_get_sounds_or_synonyms_filtered_entry(
                        l_entry, l_included_tags
                    )
                elif (
                    s_header_component in ("ld:synonyms ls:tags,s:word",)
                    and d_setting["filter synonyms"]
                ):
                    l_included_tags = d_setting["synonyms included tags"]
                    l_filtered_entry = l_get_sounds_or_synonyms_filtered_entry(
                        l_entry, l_included_tags
                    )
                elif (
                    s_header_component
                    in (
                        "ld:translations s:lang,s:lang_code,s:word",
                        "ld:translations s:lang,s:lang_code,s:sense,s:word,s:roman",
                        "ld:translations ld:senses s:lang,s:code,s:sense,s:word,s:alt,ls:tags",
                    )
                    and d_setting["filter translations"]
                ):
                    l_filtered_entry = l_get_translations_filtered_entry(l_entry)
                elif s_header_component in (
                    "ld:senses ls:raw_glosses",
                    "ld:senses ls:glosses",
                ):
                    l_filtered_entry = l_get_senses_filtered_entry(l_entry)
                else:
                    l_filtered_entry = l_entry
                s_filtered_entry = " ‖ ".join(l_filtered_entry)
                l_filtered_line_component.append(s_filtered_entry)

            if b_merge_non_empty_raw_glosses_into_glosses:
                l_filtered_line_component = (
                    l_get_merged_glosses_filtered_line_component(
                        l_filtered_line_component, l_header_component
                    )
                )
            s_filtered_line = "\t".join(l_filtered_line_component)
            l_filtered_line.append(s_filtered_line)

    s_text = s_header_after_filtering + "\n" + "\n".join(l_filtered_line) + "\n"

    u_augmented_wordlist_file = PurePath(p_augmented_wordlist_file)
    s_augmented_wordlist_file = u_augmented_wordlist_file.name
    p_filtered_augmented_wordlist_file = (
        p_filtered_augmented_wordlist_directory.joinpath(s_augmented_wordlist_file)
    )
    if (
        p_filtered_augmented_wordlist_file.is_file()
        and p_filtered_augmented_wordlist_file.read_text() == s_text
    ):
        continue
    p_filtered_augmented_wordlist_file.write_text(s_text)
