import json
from linecache import getline
from pathlib import Path, PurePath
from ruamel.yaml import YAML

yaml = YAML(typ="safe")


def s_get_s_from_d(d, s_s_key):
    if not (s_s_key in d.keys() and isinstance(d[s_s_key], str)):
        return ""
    return d[s_s_key]


def s_get_s_from_ld(d, s_ld_key, s_s_key):
    ls_value = []
    for d in d[s_ld_key]:
        if s_s_key in d.keys() and isinstance(d[s_s_key], str):
            s_value = s_get_s_from_d(d, s_s_key)
            ls_value.append(s_value)
    return " · ".join(ls_value)


def s_get_s_from_ld_from_d(d, ls_key):
    s_ld_key, s_s_key = ls_key
    if not (s_ld_key in d.keys() and isinstance(d[s_ld_key], list)):
        return ""
    return s_get_s_from_ld(d, s_ld_key, s_s_key)


def s_get_s_s_from_ld_from_d(d, ls_key):
    s_ld_key, s_s1_key, s_s2_key = ls_key
    if not (s_ld_key in d.keys() and isinstance(d[s_ld_key], list)):
        return ""
    ls_all_value = []
    for d1 in d[s_ld_key]:
        ls_value = []
        if s_s1_key in d1.keys() and isinstance(d1[s_s1_key], str):
            s = s_get_s_from_d(d1, s_s1_key)
            ls_value.append(s)
        if s_s2_key in d1.keys() and isinstance(d1[s_s2_key], str):
            s = s_get_s_from_d(d1, s_s2_key)
            ls_value.append(s)
        s_value = " · ".join(ls_value)
        ls_all_value.append(s_value)
    return " • ".join(ls_all_value)


def s_get_s_from_ls_to_s_from_ld_from_d(d, ls_key):
    s_ld_key, s_ls_key, s_s_key = ls_key
    if not (s_ld_key in d.keys() and isinstance(d[s_ld_key], list)):
        return ""
    ls_all_value = []
    for d1 in d[s_ld_key]:
        s_value_1 = ""
        if s_ls_key in d1.keys() and isinstance(d1[s_ls_key], list):
            ls = d1[s_ls_key]
            s_value_1 = " · ".join(ls)
        s_value_2 = ""
        if s_s_key in d1.keys() and isinstance(d1[s_s_key], str):
            s_value_2 += d1[s_s_key]
        else:
            continue
        s_value = s_value_1 + " : " + s_value_2
        ls_all_value.append(s_value)
    return " • ".join(ls_all_value)


def s_get_s_from_ls_from_ld_from_d(d, ls_key):
    s_ld_key, s_ls_key = ls_key
    if not (s_ld_key in d.keys() and isinstance(d[s_ld_key], list)):
        return ""
    ls_value = []
    ls_all_value = []
    for d1 in d[s_ld_key]:
        if not (s_ls_key in d1.keys() and isinstance(d1[s_ls_key], list)):
            ls_all_value.append("")
            continue
        ls_value = d1[s_ls_key]
        s_value = " · ".join(ls_value)
        ls_all_value.append(s_value)
    return " • ".join(ls_all_value)


def s_get_s_from_ld_from_ld_from_d(d, ls_key):
    s_ld1_key, s_ld2_key, s_d_key = ls_key
    if not (s_ld1_key in d.keys() and isinstance(d[s_ld1_key], list)):
        return ""
    ls_value = []
    for d1 in d[s_ld1_key]:
        if s_ld2_key in d1.keys() and isinstance(d1[s_ld2_key], list):
            s_value = s_get_s_from_ld_from_d(d1, [s_ld2_key, s_d_key])
            ls_value.append(s_value)
    return " • ".join(ls_value)


def s_get_s_from_s_s_from_ld_from_ld_from_d(d, ls_key):
    s_ld1_key, s_ld2_key, s_s1_key, s_s2_key = ls_key
    if not (s_ld1_key in d.keys() and isinstance(d[s_ld1_key], list)):
        return ""
    ls_all_value = []
    for d1 in d[s_ld1_key]:
        if s_ld2_key in d1.keys() and isinstance(d1[s_ld2_key], list):
            for d2 in d1[s_ld2_key]:
                s1_value = s_get_s_from_d(d2, s_s1_key)
                s2_value = s_get_s_from_d(d2, s_s2_key)
                s_value = s1_value + " • " + s2_value
                ls_all_value.append(s_value)
    return " ¦ ".join(ls_all_value)


def s_get_s_from_s_s_to_s_from_ld_from_d(d, ls_key):
    s_ld_key, s_s1_key, s_s2_key, s_s3_key = ls_key
    if not (s_ld_key in d.keys() and isinstance(d[s_ld_key], list)):
        return ""
    ls_all_value = []
    for d1 in d[s_ld_key]:
        ls_value_1 = []
        s_s1_value = s_get_s_from_d(d1, s_s1_key)
        ls_value_1.append(s_s1_value)
        s_s2_value = s_get_s_from_d(d1, s_s2_key)
        ls_value_1.append(s_s2_value)

        ls_value_2 = []
        s_s3_value = s_get_s_from_d(d1, s_s3_key)
        ls_value_2.append(s_s3_value)

        s_value_1 = " • ".join(ls_value_1)
        s_value_2 = " • ".join(ls_value_2)
        s_value = s_value_1 + " : " + s_value_2
        ls_all_value.append(s_value)
    return " | ".join(ls_all_value)


def s_get_s_from_s_s_to_s_s_s_from_ld_from_d(d, ls_key):
    s_ld_key, s_s1_key, s_s2_key, s_s3_key, s_s4_key, s_s5_key = ls_key
    if not (s_ld_key in d.keys() and isinstance(d[s_ld_key], list)):
        return ""
    ls_all_value = []
    for d1 in d[s_ld_key]:
        ls_value_1 = []
        s_s1_value = s_get_s_from_d(d1, [s_s1_key])
        ls_value_1.append(s_s1_value)
        s_s2_value = s_get_s_from_d(d1, [s_s2_key])
        ls_value_1.append(s_s2_value)

        ls_value_2 = []
        s_s3_value = s_get_s_from_d(d1, [s_s3_key])
        ls_value_2.append(s_s3_value)
        s_s4_value = s_get_s_from_d(d1, [s_s4_key])
        ls_value_2.append(s_s4_value)
        s_s5_value = s_get_s_from_d(d1, [s_s5_key])
        ls_value_2.append(s_s5_value)

        s_value_1 = " • ".join(ls_value_1)
        s_value_2 = " • ".join(ls_value_2)
        s_value = s_value_1 + " : " + s_value_2
        ls_all_value.append(s_value)
    return " | ".join(ls_all_value)


def s_get_s_from_s_s_to_s_s_s_ls_from_ld_from_ld_from_d(d, ls_key):
    s_ld1_key, s_ld2_key, s_s1_key, s_s2_key, s_s3_key, s_s4_key, s_s5_key, s_ls_key = (
        ls_key
    )
    if not (s_ld1_key in d.keys() and isinstance(d[s_ld1_key], list)):
        return ""
    ls_all_value = []
    for d1 in d[s_ld1_key]:
        ls_value_1 = []
        s_s1_value = s_get_s_from_d(d1, s_s1_key)
        ls_value_1.append(s_s1_value)
        s_s2_value = s_get_s_from_d(d1, s_s2_key)
        ls_value_1.append(s_s2_value)

        ls_value_2 = []
        s_s3_value = s_get_s_from_d(d1, s_s3_key)
        ls_value_2.append(s_s3_value)
        s_s4_value = s_get_s_from_d(d1, s_s4_key)
        ls_value_2.append(s_s4_value)
        s_s5_value = s_get_s_from_d(d1, s_s5_key)
        ls_value_2.append(s_s5_value)

        if not (s_ls_key in d1.keys() and isinstance(d1[s_ls_key], list)):
            ls_value_2.append("")
        else:
            s_ls_value = " · ".join(d1[s_ls_key])
            ls_value_2.append(s_ls_value)

        s_value_1 = " • ".join(ls_value_1)
        s_value_2 = " • ".join(ls_value_2)
        s_value = s_value_1 + " : " + s_value_2
        ls_all_value.append(s_value)
    return " | ".join(ls_all_value)


def s_get_dictionary_line(d_jsonl_entry, s_dictionary_content_with_type):
    s_dictionary_content = (
        s_dictionary_content_with_type.replace("ld:", "")
        .replace("ls:", "")
        .replace("s:", "")
        .replace(",", " ")
    )
    l_dictionary_content_component = s_dictionary_content.split()

    if s_dictionary_content_with_type in ("s:pos", "s:etymology_text"):
        s_dictionary_line = s_get_s_from_d(
            d_jsonl_entry, l_dictionary_content_component[0]
        )
    elif s_dictionary_content_with_type in (
        "ld:head_templates s:expansion",
        "ld:synonyms s:word",
        "ld:antonyms s:word",
        "ld:hypernyms s:word",
        "ld:hyponyms s:word",
        "ld:coordinate_terms s:word",
        "ld:sounds s:ogg_url",
        "ld:sounds s:mp3_url",
        "ld:derived s:word",
    ):
        s_dictionary_line = s_get_s_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    elif s_dictionary_content_with_type in (
        "ld:synonyms ls:tags,s:word",
        "ld:forms ls:tags,s:form",
        "ld:sounds ls:tags,s:ipa",
        "ld:sounds ls:raw_tags,s:ipa",
        "ld:sounds ls:tags,s:zh-pron",
        "ld:sounds ls:raw_tags,s:zh_pron",
        "ld:related ls:tags,s:word",
    ):
        s_dictionary_line = s_get_s_from_ls_to_s_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    elif s_dictionary_content_with_type in ("ld:derived s:word,s:roman"):
        s_dictionary_line = s_get_s_s_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    elif s_dictionary_content_with_type in (
        "ld:senses ls:raw_glosses",
        "ld:senses ls:glosses",
    ):
        s_dictionary_line = s_get_s_from_ls_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    elif s_dictionary_content_with_type in (
        "ld:senses ld:synonyms s:word",
        "ld:senses ld:antonyms s:word",
        "ld:senses ld:hypernyms s:word",
        "ld:senses ld:hyponyms s:word",
        "ld:senses ld:coordinate_terms s:word",
        "ld:senses ld:examples s:text",
    ):
        s_dictionary_line = s_get_s_from_ld_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    elif s_dictionary_content_with_type in (
        "ld:senses ld:examples s:text,s:translation",
        "ld:senses ld:examples s:text,s:expansion",
        "ld:senses ld:examples s:text,s:english",
    ):
        s_dictionary_line = s_get_s_from_s_s_from_ld_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    elif s_dictionary_content_with_type in (
        "ld:translations s:lang,s:lang_code,s:word"
    ):
        s_dictionary_line = s_get_s_from_s_s_to_s_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    elif s_dictionary_content_with_type in (
        "ld:translations s:lang,s:lang_code,s:sense,s:word,s:roman"
    ):
        s_dictionary_line = s_get_s_from_s_s_to_s_s_s_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    elif s_dictionary_content_with_type in (
        "ld:translations ld:senses s:lang,s:code,s:sense,s:word,s:alt,ls:tags"
    ):
        s_dictionary_line = s_get_s_from_s_s_to_s_s_s_ls_from_ld_from_ld_from_d(
            d_jsonl_entry, l_dictionary_content_component
        )
    else:
        s_dictionary_line = ""
    return s_dictionary_line.replace("\n", " \\ ")


def l_get_tsv_line_and_missing_word(l_word):
    i_word_amount = len(l_word)
    l_missing_word = []
    for i_word_index in range(i_word_amount):
        s_word = l_word[i_word_index]
        i_word_length = len(s_word)
        l_key = [i_word_length] + list(s_word)
        d_current_dictionary = d_jsonl_line_index_by_word_length_and_character

        l_jsonl_line_index = []
        i_key_amount = len(l_key)
        for i_key_index in range(i_key_amount):
            key = l_key[i_key_index]
            if key in d_current_dictionary.keys():
                if i_key_index < i_key_amount - 1:
                    d_current_dictionary = d_current_dictionary[key]
                else:
                    l_jsonl_line_index = d_current_dictionary[key]
                    break
            else:
                l_missing_word.append(s_word)
                break
        if l_missing_word != [] and s_word == l_missing_word[-1]:
            continue

        d_line = {}
        for s_dictionary_content_with_type in l_dictionary_content_with_type:
            d_line.update({s_dictionary_content_with_type: []})
        d_line.update({"s:word": s_word})

        l_jsonl_entry = []
        for i_jsonl_line_index in l_jsonl_line_index:
            s_jsonl_line = getline(s_dictionary_file_path, i_jsonl_line_index)
            d_jsonl_entry = json.loads(s_jsonl_line)
            l_jsonl_entry.append(d_jsonl_entry)

        for d_jsonl_entry in l_jsonl_entry:
            for s_dictionary_content_with_type in l_dictionary_content_with_type:
                if s_dictionary_content_with_type == "s:word":
                    continue
                s_dictionary_line = s_get_dictionary_line(
                    d_jsonl_entry,
                    s_dictionary_content_with_type,
                )
                d_line[s_dictionary_content_with_type].append(s_dictionary_line)

        l_line = []
        for dictionary_value in d_line.values():
            if isinstance(dictionary_value, list):
                dictionary_value = " ‖ ".join(dictionary_value)
            l_line.append(dictionary_value)
        s_line = "\t".join(l_line)
        l_tsv_line.append(s_line)
    return [l_tsv_line, l_missing_word]


s_setting_file = "setting.yaml"
p_setting_file = Path(s_setting_file)
s_setting_text = p_setting_file.read_text()
d_setting = yaml.load(s_setting_text)

s_dictionary_directory = d_setting["dictionary directory"]
p_dictionary_directory = Path(s_dictionary_directory)
s_jsonl_file = d_setting["JSON file"]
s_dictionary_file_path = s_dictionary_directory + "/" + s_jsonl_file

with open(s_dictionary_file_path) as w_jsonl_file:
    i_jsonl_line_amount = 0
    for s_jsonl_line in w_jsonl_file:
        i_jsonl_line_amount += 1

i_jsonl_line_index = 0
d_jsonl_line_index_by_word_length_and_character = {}
with open(s_dictionary_file_path) as w_jsonl_file:
    for s_jsonl_line in w_jsonl_file:
        if (
            i_jsonl_line_index + 1
        ) % 10000 == 0 or i_jsonl_line_index == i_jsonl_line_amount - 1:
            s_progress = str(i_jsonl_line_index + 1) + "/" + str(i_jsonl_line_amount)
            print(s_progress)
        i_jsonl_line_index += 1

        d_jsonl_entry = json.loads(s_jsonl_line)
        s_lang_code = d_setting["Wiktionary lang_code"]
        if (
            s_lang_code != s_get_s_from_d(d_jsonl_entry, "lang_code")
            or "kanji" in d_jsonl_entry.keys()
            or (
                "pos" in d_jsonl_entry.keys()
                and d_jsonl_entry["pos"] == "soft-redirect"
            )
        ):
            continue
        s_word = s_get_s_from_d(d_jsonl_entry, "word")
        i_word_length = len(s_word)
        l_key = [i_word_length] + list(s_word)

        d_current_dictionary = d_jsonl_line_index_by_word_length_and_character
        i_key_amount = len(l_key)
        for i_key_index in range(i_key_amount):
            key = l_key[i_key_index]
            if i_key_index < i_key_amount - 1:
                if key not in d_current_dictionary.keys():
                    d_current_dictionary[key] = {}
                d_current_dictionary = d_current_dictionary[key]
            else:
                if key in d_current_dictionary.keys():
                    d_current_dictionary[key].append(i_jsonl_line_index)
                else:
                    d_current_dictionary[key] = [i_jsonl_line_index]

s_wordlist_directory = d_setting["wordlist directory"]
p_wordlist_directory = Path(s_wordlist_directory)
p_wordlist_directory.mkdir(exist_ok=True)

b_keep_missing_word = d_setting["keep missing-word"]
if b_keep_missing_word:
    s_missing_word_directory = d_setting["missing-word directory"]
    p_missing_word_directory = Path(s_missing_word_directory)
    p_missing_word_directory.mkdir(exist_ok=True)

s_augmented_wordlist_directory = d_setting["augmented wordlist directory"]
p_augmented_wordlist_directory = Path(s_augmented_wordlist_directory)
p_augmented_wordlist_directory.mkdir(exist_ok=True)

l_dictionary_content_with_type = d_setting["dictionary content with type"]
s_tsv_header = "\t".join(l_dictionary_content_with_type)

i_word_group_index = l_dictionary_content_with_type.index("s:word")

for p_wordlist_file in p_wordlist_directory.rglob("*.txt"):
    u_wordlist_file = PurePath(p_wordlist_file)
    s_wordlist_file_name = u_wordlist_file.stem
    s_augmented_wordlist_file = s_wordlist_file_name + ".tsv"
    p_augmented_wordlist_file = p_augmented_wordlist_directory.joinpath(
        s_augmented_wordlist_file
    )
    s_wordlist_file = u_wordlist_file.name
    p_missing_word_file = p_missing_word_directory.joinpath(s_wordlist_file)

    s_tsv_text = ""
    l_tsv_line = []

    s_wordlist_text = p_wordlist_file.read_text().rstrip("\n")
    l_word = s_wordlist_text.splitlines()
    l_tsv_line, l_missing_word = l_get_tsv_line_and_missing_word(l_word)

    s_tsv_line = "\n".join(l_tsv_line)
    s_augmented_wordlist_text = s_tsv_header + "\n" + s_tsv_line + "\n"
    if (
        p_augmented_wordlist_file.is_file()
        and s_augmented_wordlist_text == p_augmented_wordlist_file.read_text()
    ):
        continue
    p_augmented_wordlist_file.write_text(s_augmented_wordlist_text)

    s_missing_word_text = "\n".join(l_missing_word) + "\n"
    if (
        p_missing_word_file.is_file()
        and s_missing_word_text == p_missing_word_file.read_text()
    ):
        continue
    p_missing_word_file.write_text(s_missing_word_text)

    s_tsv_text = ""
    l_tsv_line = []
