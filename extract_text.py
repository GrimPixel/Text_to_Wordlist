from pathlib import Path, PurePath
from ruamel.yaml import YAML
from simplemma import lemmatize
import spacy


def d_get_stopword_by_length_and_character(l_word, d_stopword_by_length_and_character):
    for s_word in l_word:
        i_word_length = len(s_word)
        l_key = [i_word_length] + list(s_word)

        d_current_dictionary = d_stopword_by_length_and_character
        i_key_length = len(l_key)
        for i_key_index in range(i_key_length):
            key = l_key[i_key_index]
            if i_key_index < len(l_key) - 1:
                if key not in d_current_dictionary.keys():
                    d_current_dictionary[key] = {}
                d_current_dictionary = d_current_dictionary[key]
            else:
                d_current_dictionary[key] = ""
    return d_current_dictionary


yaml = YAML(typ="safe")

s_setting_file = "setting.yaml"
p_setting_file = Path(s_setting_file)
s_setting_text = p_setting_file.read_text()
d_setting = yaml.load(s_setting_text)

s_stopword_directory = Path(d_setting["stopword directory"])
d_stopword_by_length_and_character = {}
p_stopword_directory = s_stopword_directory
for p_stopword_file in p_stopword_directory.rglob("*"):
    s_stopword_file_path = str(p_stopword_file)
    if s_stopword_file_path.endswith(".txt"):
        s_stopword_text = p_stopword_file.read_text().rstrip("\n")
        l_word = s_stopword_text.split("\n")
    elif s_stopword_file_path.endswith(".tsv"):
        d_stopword_by_length_and_character.update(
            d_get_stopword_by_length_and_character(
                l_word, d_stopword_by_length_and_character
            )
        )

s_text_directory = d_setting["text directory"]
p_text_directory = Path(s_text_directory)
p_text_directory.mkdir(exist_ok=True)
s_wordlist_directory = d_setting["wordlist directory"]

s_spacy_pipeline = d_setting["spaCy pipeline"]
s_simplemma_language_code = d_setting["Simplemma language code"]
for p_file in p_text_directory.rglob("*.txt"):
    s_text = p_file.read_text()

    l_word = []
    spacy_pipeline = spacy.load(s_spacy_pipeline)
    spacy_doc = spacy_pipeline(s_text)
    for spacy_token in spacy_doc:
        s_token = str(spacy_token)
        if s_token in l_word:
            continue
        s_spacy_token = str(spacy_token)
        l_word.append(s_spacy_token)

    l_unique_word = []
    for s_word in l_word:
        if s_word not in l_unique_word:
            l_unique_word.append(s_word)

    l_filtered_unique_word = []
    for s_unique_word in l_unique_word:
        d_stopword_current_dictionary = d_stopword_by_length_and_character
        if s_unique_word.isnumeric() or s_unique_word in (
            "\0",
            "\a",
            "\b",
            "\t",
            "\n",
            "\v",
            "\f",
            "\r",
        ):
            continue
        i_length = len(s_unique_word)
        l_stopword_length_and_character = [i_length] + list(s_unique_word)
        for s_stopword_length_and_character in l_stopword_length_and_character:
            if s_stopword_length_and_character in d_stopword_current_dictionary.keys():
                d_stopword_current_dictionary = d_stopword_current_dictionary[
                    s_stopword_length_and_character
                ]
                continue
            l_filtered_unique_word.append(s_unique_word)
            break

    l_lemmatised_filtered_unique_word = l_filtered_unique_word
    for s_filtered_unique_word in l_filtered_unique_word:
        s_lemma = lemmatize(s_filtered_unique_word, lang=s_simplemma_language_code)
        if s_lemma in l_lemmatised_filtered_unique_word:
            continue
        l_lemmatised_filtered_unique_word.append(s_lemma)
    l_lemmatised_filtered_unique_word.sort()
    s_wordlist_text = "\n".join(l_lemmatised_filtered_unique_word).strip() + "\n"

    u_file = PurePath(p_file)
    s_file = u_file.name
    p_wordlist_directory = Path(s_wordlist_directory)
    p_wordlist_file = p_wordlist_directory.joinpath(s_file)

    p_wordlist_directory.mkdir(exist_ok=True)
    if p_wordlist_file.is_file() and p_wordlist_file.read_text() == s_wordlist_text:
        continue
    p_wordlist_file.write_text(s_wordlist_text)
